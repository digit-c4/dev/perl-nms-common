# perl-nms-common - Overview
The NMSLogging class provides a standardized method for logging messages in Perl applications. It includes methods for logging messages with different severity levels and ensures that logs follow a consistent format for easier integration with tools like Splunk.

## Logging
Copy **src/NMSCommon/NMSLogging.pm** into your project structure like this:
```
project-root/
|- src/
| |- lib
| |- |- NMSLogging.pm
| |- main.pl
```

And use it this way:

```perl
# project-root/src/main.pl
use strict;
use warnings;
use lib "./lib";
use Logger;

my $logger = Logger->new(
    owner_type => "team",
    owner_name => "dev",
    app_type   => "app",
    app_name   => "logging-example"
);

$logger->log("INFO", "Hello World.");
$logger->log("CRITICAL", "HELP!");
$logger->log("OK", "Nothing to see here! Please disperse!");
$logger->metaprint("INFO", "This is an informational message.");
$logger->print("INFO", "This is an informational message.");

```

# Migration/Preparation/Execution Steps
Preparation:

Review the existing logging setup in your Perl application.
Ensure you have copied NMSLogging.pm to the appropriate directory in your project.

# Migration:

Replace existing logging calls with the NMSLogging methods.
Example replacement:

# Old logging method
old_log("INFO", "Old log message");

# New logging method
$logger->log("INFO", "New log message using NMSLogging.");


