# package NMScommon::NMSLogging;
package NMSLogging;

use strict;
use warnings;
use Data::Dumper;
use POSIX qw(strftime);
use Term::ANSIColor;

# Constants
my $OWNER_TYPE  = "team";
my $OWNER_NAME  = "dev";
my $APP_TYPE    = "app";

sub new {
  my ($class, %args) = @_;
  unless (exists $args{app_name}) {
    die "Parameter 'app_name' is required for class 'NMSLogging'";
  }
  my $self = {
    owner_type  => $args{owner_type}    || $OWNER_TYPE,
    owner_name  => $args{owner_name}    || $OWNER_NAME,
    app_type    => $args{app_type}      || $APP_TYPE,
    app_name    => $args{app_name},
  };
  bless $self, "NMSLogging";
  return $self;
}


sub log {
    my ($self, $level, $message) = @_;
    my $currentdate = strftime "%Y-%m-%dT%H:%M:%S", localtime;
    my $currentpid = $$;
    my $header = "[$currentdate][$currentpid][PLOGGER][$level][$self->{owner_type}:$self->{owner_name}][$self->{app_type}:$self->{app_name}]";
    my $color = 'white';
    my $web_color = '';
    my $web_mode = 0;

    # TODO: get this [ugly](https://i.imgflip.com/9x4ew.jpg) block in its own subroutine...
    # Section : Display logic (inspired from metaprint).
    if ( ( $level =~ /sql/i ) || ( $level =~ /dieget/i ) ) {
        $message =~ s/\r//g;
        $message =~ s/\n/ /g;
        $message =~ s/\s+/ /g;
        $message =~ s/select\s/SELECT /gi;
        $message =~ s/\sfrom\s/ FROM /gi;
        $message =~ s/\swhere\s/ WHERE /gi;
        $message =~ s/\sand\s/ AND /gi;
        $message =~ s/\sjoin\s/ JOIN /gi;
        $message =~ s/insert\s/INSERT /gi;
        $message =~ s/update\s/UPDATE /gi;
        $color = 'white';
    } elsif ( $level =~ /ok/i ) {
        $color     = 'green';
        $web_color = 'igr';
    } elsif ( $level =~ /info/i ) {
        $color = 'white';
    } elsif ( $level =~ /warn/i ) {
        $color     = 'yellow';
        $web_color = 'ior';
    } elsif ( $level =~ /error/i ) {
        $color     = 'red';
        $web_color = 'ire';
    } elsif ( $level =~ /critical/i ) {
        $color     = 'red';
        $web_color = 'ire';
    }
    if ( defined( $main::cli_mode ) && $main::cli_mode ) {
        $web_mode = 0;
    }
    if ( defined( $main::stderr ) && $main::stderr ) {
        print STDERR $header . " - "
            . ( ( caller( 1 ) )[3] ? ( caller( 1 ) )[3] : '' ) . " - "
            . ( ( caller( 0 ) )[2] ? 'L' . ( caller( 0 ) )[2] . ' - ' : '' )
            . $message . "\n";
    } elsif ( ( $web_mode ) && ( $web_color ne '' ) ) {
        print $header
            . " - [<div class='$web_color'>"
            . uc( $level )
            . "</div>] - "
            . ( ( caller( 1 ) )[3] ? ( caller( 1 ) )[3] : '' ) . " - "
            . ( ( caller( 0 ) )[2] ? 'L' . ( caller( 0 ) )[2] . ' - ' : '' )
            . $message
            . nl();
    } elsif ( $web_mode ) {
        print $header . " - " . ( ( caller( 1 ) )[3] ? ( caller( 1 ) )[3] : '' ) . " - " . ( ( caller( 0 ) )[2] ? 'L' . ( caller( 0 ) )[2] . ' - ' : '' ) . $message . nl();
    } elsif ( defined( $main::no_color_mode ) && ( $main::no_color_mode ) ) {
        print $header . " - " . ( ( caller( 1 ) )[3] ? ( caller( 1 ) )[3] : '' ) . " - " . ( ( caller( 0 ) )[2] ? 'L' . ( caller( 0 ) )[2] . ' - ' : '' ) . $message . "\n";
    } else {
        print STDERR colored("$header $message\n", $color);
        print color 'reset';
    }
}

sub metaprint {
    my ($self, $level, $message) = @_;
    my $currentdate = strftime "%Y-%m-%dT%H:%M:%S", localtime;
    my $currentpid = $$;
    my $header = "[$currentdate][$currentpid][PLOGGER][$level][$self->{owner_type}:$self->{owner_name}][$self->{app_type}:$self->{app_name}]";
    metaprint($level, "$header $message\n");
}

sub print {
    my ($self, $level, $message) = @_;
    my $currentdate = strftime "%Y-%m-%dT%H:%M:%S", localtime;
    my $currentpid = $$;
    my $header = "[$currentdate][$currentpid][PLOGGER][$level][$self->{owner_type}:$self->{owner_name}][$self->{app_type}:$self->{app_name}]";
    print "$header $message\n";
}

1;
